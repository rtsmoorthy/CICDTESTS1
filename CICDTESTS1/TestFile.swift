//
//  TestFile.swift
//  CICDTESTS1
//
//  Created by Ranjith on 08/10/2021.
//

import Foundation
class PriceCalculator {
    
    func calculateTicketPrice(tickets: Int)-> Int{
        guard tickets > 0 else{
            return 0
        }
        guard tickets <= 100 else{
            return 0
        }
        return tickets * 100
    }
}
